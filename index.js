let apikey = document.getElementById("keyinput").value;

function getLatLon(text) {
  const dim = Number(document.getElementById("diminput").value);

  let parts = text.split(",");
  console.log(parts.length);
  console.log(/^[0-9,.-]*$/.test(text));
  console.log(parts != "");

  if (parts.length == 2 && parts != "") {
    apikey = document.getElementById("keyinput").value;
    document.getElementById("progressbar").value = document.getElementById(
      "progressbar"
    ).value = 0;
    document.querySelector("input#textinput").classList.remove("is-danger");
    document.querySelector("input#textinput").classList.add("is-success");
    document.getElementById("inputTextControl").classList.add("is-loading");

    let lat = Number(parts[0]);
    let lon = Number(parts[1]);
    let date = document.getElementById("dateinput").value;

    document.getElementById(
      "topleft"
    ).src = `https://api.nasa.gov/planetary/earth/imagery?lon=${encodeURI(
      lon - dim
    )}&lat=${encodeURI(lat + dim)}&dim=${encodeURI(
      dim
    )}&date=${date}&api_key=${encodeURI(apikey)}`;
    document.getElementById(
      "midleft"
    ).src = `https://api.nasa.gov/planetary/earth/imagery?lon=${encodeURI(
      lon - dim
    )}&lat=${encodeURI(lat)}&dim=${encodeURI(
      dim
    )}&date=${date}&api_key=${encodeURI(apikey)}`;
    document.getElementById(
      "botleft"
    ).src = `https://api.nasa.gov/planetary/earth/imagery?lon=${encodeURI(
      lon - dim
    )}&lat=${encodeURI(lat - dim)}&dim=${encodeURI(
      dim
    )}&date=${date}&api_key=${encodeURI(apikey)}`;
    document.getElementById(
      "topcenter"
    ).src = `https://api.nasa.gov/planetary/earth/imagery?lon=${encodeURI(
      lon
    )}&lat=${encodeURI(lat + dim)}&dim=${encodeURI(
      dim
    )}&date=${date}&api_key=${encodeURI(apikey)}`;
    document.getElementById(
      "midcenter"
    ).src = `https://api.nasa.gov/planetary/earth/imagery?lon=${encodeURI(
      lon
    )}&lat=${encodeURI(lat)}&dim=${encodeURI(
      dim
    )}&date=${date}&api_key=${encodeURI(apikey)}`;
    document.getElementById(
      "botcenter"
    ).src = `https://api.nasa.gov/planetary/earth/imagery?lon=${encodeURI(
      lon
    )}&lat=${encodeURI(lat - dim)}&dim=${encodeURI(
      dim
    )}&date=${date}&api_key=${encodeURI(apikey)}`;
    document.getElementById(
      "topright"
    ).src = `https://api.nasa.gov/planetary/earth/imagery?lon=${encodeURI(
      lon + dim
    )}&lat=${encodeURI(lat + dim)}&dim=${encodeURI(
      dim
    )}&date=${date}&api_key=${encodeURI(apikey)}`;
    document.getElementById(
      "midright"
    ).src = `https://api.nasa.gov/planetary/earth/imagery?lon=${encodeURI(
      lon + dim
    )}&lat=${encodeURI(lat)}&dim=${encodeURI(
      dim
    )}&date=${date}&api_key=${encodeURI(apikey)}`;
    document.getElementById(
      "botright"
    ).src = `https://api.nasa.gov/planetary/earth/imagery?lon=${encodeURI(
      lon + dim
    )}&lat=${encodeURI(lat - dim)}&dim=${encodeURI(
      dim
    )}&date=${date}&api_key=${encodeURI(apikey)}`;

    document.getElementById("inputTextControl").classList.remove("is-loading");
    return;
  } else {
    document.querySelector("input#textinput").classList.remove("is-success");
    document.getElementById("inputTextControl").classList.remove("is-loading");
    document.querySelector("input#textinput").classList.add("is-danger");
    return;
  }
}

document.querySelector("button#mybutton").addEventListener("click", () => {
  getLatLon(document.querySelector("input#textinput").value);
});

document
  .getElementById("topleft")
  .addEventListener("load", incrimentProgress, false);
document
  .getElementById("midleft")
  .addEventListener("load", incrimentProgress, false);
document
  .getElementById("botleft")
  .addEventListener("load", incrimentProgress, false);
document
  .getElementById("topcenter")
  .addEventListener("load", incrimentProgress, false);
document
  .getElementById("midcenter")
  .addEventListener("load", incrimentProgress, false);
document
  .getElementById("botcenter")
  .addEventListener("load", incrimentProgress, false);
document
  .getElementById("botcenter")
  .addEventListener("load", incrimentProgress, false);
document
  .getElementById("topright")
  .addEventListener("load", incrimentProgress, false);
document
  .getElementById("midright")
  .addEventListener("load", incrimentProgress, false);
ocument
  .getElementById("botright")
  .addEventListener("load", incrimentProgress, false);

function incrimentProgress() {
  document.getElementById("progressbar").value = document.getElementById(
    "progressbar"
  ).value += 1;
}
