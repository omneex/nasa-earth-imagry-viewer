# NASA Earth Imagry Viewer
A very basic GUI program using **NodeJS, ElectronJS, and Bulma CSS** that allows user interaction with the **NASA Earth API**.
## How to use this project
### From source
1.
`git clone https://gitlab.com/omneex/nasa-earth-imagry-viewer`

2.
`cd nasa-earth-imagry-viewer`

3.
`npm install`

4.
`npm start`

There is no key provided with this project, you can use the limited DEMO_KEY that is default or provide your own.
